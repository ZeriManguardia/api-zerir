////////////////////////////////////////////////////////
//	Author: Zeri Jod C. Manguardia.					  //
//	Allowed for re-use. Feel free to distribute       //
//   Timer implementation using <Time.h>		      //
////////////////////////////////////////////////////////

#include "Timer.h"
//GO
namespace Timer{


	_timer::_timer(){};

	void _timer::startTimer(){
		previousTime = startTime = currentTime = clock();
	};

	double _timer::getElapsedTime(){
		return ((float)currentTime)/CLOCKS_PER_SEC;
	}

	void _timer::updateClock(){
		currentTime = clock() - previousTime;
		std::cout << getElapsedTime() << std::endl;
	}

	void _timer::resetTimer(){
		
		previousTime = clock();
		
	}



	/*IMPLEMENTATION 

	using namespace Timer;

void myImplementation(){

	_timer * timerInstance = new _timer();
	timerInstance->startTimer();
  for(;;){  or while(true)

	  timerInstance->updateClock();
	  std::system("cls"); 
	  
	if(timerInstance->getElapsedTime() >= 2.0f){
		 timerInstance->resetTimer();
		 break;
		 }
		 
  }

}




	*/
}