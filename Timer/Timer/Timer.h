////////////////////////////////////////////////////////
//	Author: Zeri Jod C. Manguardia.					  //
//	Allowed for re-use. Feel free to distribute       //
//   Timer implementation using <Time.h>		      //
////////////////////////////////////////////////////////


#ifndef _TIMER_H_
#define _TIMER_H_
#pragma once
#include <iostream>
#include <stdio.h>
#include <time.h>

namespace Timer{


extern enum CLOCKMODE{

CONTINUOUS, INTERVAL, STOP_TIME

};

class _timer{

public:
	_timer();

	void startTimer();

	double getElapsedTime();

	void updateClock();

	void resetTimer();

private:
	clock_t currentTime;
	clock_t previousTime;
	clock_t startTime;
	
};









};
#endif
